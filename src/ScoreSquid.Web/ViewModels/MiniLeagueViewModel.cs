﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreSquid.Web.ViewModels
{
    public class MiniLeagueViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Pin { get; set; }
    }
}